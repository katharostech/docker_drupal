#!/bin/bash

# Download Composer Installer
wget -O composer-setup.php https://getcomposer.org/installer

# Validate download
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")
EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)

#if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
#then
    #>&2 echo 'ERROR: Invalid installer signature'
    #rm composer-setup.php
    #exit 1
#fi

# Run the installer
php composer-setup.php
RESULT=$?

if [ -f /composer.phar ]
then
    echo "Moving composer.phar to /usr/bin/composer"
    mv /composer.phar /usr/bin/composer
fi

# Clean up
rm composer-setup.php

exit $RESULT
