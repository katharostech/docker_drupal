# Using sendfile can cause caching problems when using Virtualbox with a shared volume. Set NGINX_SENDFILE to "off" if you have problems.
NGINX_SENDFILE=${NGINX_SENDFILE-"on"}

# Substitute the value into the configuration file
sed -i s/\$NGINX_SENDFILE/${NGINX_SENDFILE}/ /etc/nginx/conf.d/drupal.conf
