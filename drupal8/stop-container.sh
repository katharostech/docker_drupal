#!/bin/bash

# Return value
RETURN=0

# Stop nginx
kill -SIGQUIT $(cat /run/nginx.pid)
NGINX_EXIT=$?

# Add exit code
RETURN=$(($RETURN + 2 * NGINX_EXIT))

# Stop php-fpm
kill -SIGQUIT $(cat /run/php/php7.0-fpm.pid)
PHP_FPM_EXIT=$? 

# Add exit code
RETURN=$(($RETURN + 3 * PHP_FPM_EXIT))

exit $RETURN
