# Download drush
wget --no-verbose https://s3.amazonaws.com/files.drush.org/drush.phar -O drush

# Test your install.
php drush core-status

# Make `drush` executable as a command from anywhere.
chmod +x drush
mv drush /usr/local/bin

# Enrich the bash startup file with completion and aliases.
drush init -y
