#!/bin/bash

# Start cron with logs for all events
cron -L 15

# Start php-fpm
php-fpm7.0

# Start nginx
nginx
