#!/bin/bash

# Trap SIGTERM
trap '/stop-container.sh; exit $?' SIGTERM SIGINT

# Run our drupal configuration script
/configure-drupal.sh

# Run our nginx configuration script
/configure-nginx.sh

# Start our applications
/start-container.sh

# Wait for signal
while true; do :; done
