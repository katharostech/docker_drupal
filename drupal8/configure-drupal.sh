# Config defaults
INSTALL_DIR=/usr/share/nginx/html
DEFAULT_SETTINGS_FILE=${INSTALL_DIR}/sites/default/default.settings.php
SETTINGS_FILE=${INSTALL_DIR}/sites/default/settings.php
SALT_FILE=/usr/share/nginx/drupal-salt.txt
SYNC_DIRECTORY=/usr/share/nginx/drupal-config/sync

# If there is drupal configuration passed into the container and there
# is no existing settings file
# NOTE: $DB_NAME has been chosen to be representative of passed in configuration
if [ "$DB_NAME" -a ! -f $SETTINGS_FILE ];
then
    
    # Set config defaults
    DB_DRIVER=${DB_DRIVER-mysql}
    DB_USER=${DB_USER-root}
    DB_PORT=${DB_PORT-3306}
    DB_COLLATION=${DB_COLL-utf8mb4_general_ci}

    # Copy the default settings file
    cp $DEFAULT_SETTINGS_FILE $SETTINGS_FILE

    ####
    # Add database configuration
    ####

    if [ "$DB_DRIVER" = "sqlite" ];
    then
        echo "
\$databases['default']['default'] = array (
    'database' => '${DB_NAME}',
    'prefix' => '${DB_PREFIX}',
    'driver' => 'sqlite',
);" >> $SETTINGS_FILE

    elif [ "$DB_DRIVER" = "mysql" ];
    then
        echo "
\$databases['default']['default'] = array (
    'database' => '${DB_NAME}',
    'username' => '${DB_USER}',
    'password' => '${DB_PASSWORD}',
    'host' => '${DB_HOST}',
    'port' => '${DB_PORT}',
    'driver' => 'mysql',
    'prefix' => '${DB_PREFIX}',
    'collation' => '${DB_COLLATION}',
);" >> $SETTINGS_FILE
    fi

    ####
    # Add the hash salt to the drupal configuration
    ####

    # If the salt file doesn't exit
    if [ ! -f $SALT_FILE ];
    then
        # Create salt if not passed in
        if [ -z "$HASH_SALT" ];
        then
            HASH_SALT=$(date | sha256sum | awk '{ print $1 }')
        fi

        # Write the salt to the salt file
        echo $HASH_SALT >> $SALT_FILE
    fi
    
    # Print config to the drupal config file
    echo "\$settings['hash_salt'] = file_get_contents('/usr/share/nginx/drupal-salt.txt');" >> $SETTINGS_FILE

    ####
    # Add Trusted Host Config
    ####
    
    if [ -n "$TRUSTED_HOST_PATTERNS" ];
    then
        echo "\$settings['trusted_host_patterns'] = array( $TRUSTED_HOST_PATTERNS );" >> $SETTINGS_FILE
    fi

    ####
    # Configure proxy settings
    ####

    if [ -n "$HTTP_PROXY" ];
    then
        echo "\$settings['http_client_config']['proxy']['http'] = '$HTTP_PROXY';" >> $SETTINGS_FILE

    elif [ -n "$http_proxy" ];
    then
        echo "\$settings['http_client_config']['proxy']['http'] = '$http_proxy';" >> $SETTINGS_FILE
    fi

    if [ -n "$HTTPS_PROXY" ];
    then
        echo "\$settings['http_client_config']['proxy']['https'] = '$HTTPS_PROXY';" >> $SETTINGS_FILE

    elif [ -n "$https_proxy" ];
    then
        echo "\$settings['http_client_config']['proxy']['https'] = '$https_proxy';" >> $SETTINGS_FILE
    fi

    if [ -n "$NO_PROXY" ];
    then
        echo "\$settings['http_client_config']['proxy']['no'] = '$NO_PROXY';" >> $SETTINGS_FILE

    elif [ -n "$no_proxy" ];
    then
        echo "\$settings['http_client_config']['proxy']['no'] = [$no_proxy];" >> $SETTINGS_FILE
    fi

    ####
    # Local Configuration File
    #
    # Load the settings.local.php file (for development environments)
    ####

    if [ "$LOCAL_SETTINGS" = "true" ];
    then
        echo "if (file_exists(__DIR__ . '/settings.local.php')) { include __DIR__ . '/settings.local.php'; }" >> $SETTINGS_FILE
    fi
fi

####
# Configure the cron job
####

if [ -n "$CRON_SCHEDULE" ];
then
    echo "$CRON_SCHEDULE www-data /usr/local/bin/drush --root=/usr/share/nginx/html --quiet cron" >> /etc/cron.d/drupal
fi
