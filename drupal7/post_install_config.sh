CONFIG_FILE="/etc/nginx/conf.d/drupal.conf"

sed -i "/fastcgi_pass/s/php_host/${PHP_HOST}/g" ${CONFIG_FILE}
sed -i "/fastcgi_pass/s/php_port/${PHP_PORT}/g" ${CONFIG_FILE}
