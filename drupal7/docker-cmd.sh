# Run our configuration substitution script
sh /post_install_config.sh

# Run the php server
#
# The -R parameter is to allow the process to run as root which
# is disabled by default.
php-fpm -R

# Run Nginx web server
echo "Starting Nginx Web Server"
exec nginx
